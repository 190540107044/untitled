import 'package:flutter/material.dart';
import 'package:untitled/listUser.dart';
import 'package:untitled/dashboard.dart';
import 'package:untitled/favourite.dart';
import 'package:untitled/listUser.dart';
import 'package:untitled/search.dart';

import 'addEdit.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home:  AppScreen(),
    );
  }
}
class AppScreen extends StatefulWidget{
  @override
  State<AppScreen> createState() => _AppScreenState();
}


class _AppScreenState extends State<AppScreen> {

  int pageIndex = 0;
  Color primaryColor = const Color(0xFF004a99);
  List pages = [
    DashBoard(),
    SearchScreen(),
    HomePage(),
    FavoriteScreen(),
    AddEditUser(),
  ];

  void _onItemTapped(int value) {
    setState(() {
      pageIndex = value;
    });
  }

@override
Widget build(BuildContext context) {
  return Scaffold(
      body: pages[pageIndex],
      bottomNavigationBar: Container(
        height: 70,
        decoration: BoxDecoration(
          color: primaryColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50),
              topRight: Radius.circular(50)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            SizedBox(width: 5,),
            IconButton(
              enableFeedback: false,
              onPressed: () {
                setState(() {
                  pageIndex = 0;
                  primaryColor = Color(0xFF004a99);
                });
              },
              icon: pageIndex == 0
                  ? const Icon(
                Icons.home_outlined,
                color: Color(0xFFc9eeff),
                size: 35,
              )
                  : const Icon(
                Icons.home_outlined,
                color: Colors.grey,
                size: 30,
              ),
            ),
            IconButton(
              enableFeedback: false,
              onPressed: () {
                setState(() {
                  pageIndex = 1;
                  primaryColor = Color(0xFF402d66);
                });
              },
              icon: pageIndex == 1
                  ? const Icon(
                Icons.search_outlined,
                color: Color(0xFFd9c6ff),
                size: 35,
              )
                  : const Icon(
                Icons.search_outlined,
                color: Colors.grey,
                size: 30,
              ),
            ),
            IconButton(
              enableFeedback: false,
              onPressed: () {
                setState(() {
                  pageIndex = 2;
                  primaryColor = Color(0xFF65501c);
                });
              },
              icon: pageIndex == 2
                  ? const Icon(
                Icons.format_list_bulleted_outlined,
                color: Color(0xFFfee9b5),
                size: 35,
              )
                  : const Icon(
                Icons.format_list_bulleted_outlined,
                color: Colors.grey,
                size: 30,
              ),
            ),
            IconButton(
              enableFeedback: false,
              onPressed: () {
                setState(() {
                  pageIndex = 3;
                  primaryColor = Color(0xFF601e1f);
                });
              },
              icon: pageIndex == 3
                  ? const Icon(
                Icons.star_border,
                color: Color(0xFFf9b7b8),
                size: 35,
              )
                  : const Icon(
                Icons.star_border,
                color: Colors.grey,
                size: 30,
              ),
            ),
            IconButton(
              enableFeedback: false,
              onPressed: () {
                setState(() {
                  pageIndex = 4;
                  primaryColor = Color(0xFF1f584e);
                });
              },
              icon: pageIndex == 4
                  ? const Icon(
                Icons.person_add_outlined,
                color: Color(0xFFb8f1e7),
                size: 35,
              )
                  : const Icon(
                Icons.person_add_outlined,
                color: Colors.grey,
                size: 30,
              ),
            ),
            SizedBox(width: 5,),
          ],
        ),
      )
  );
}
}
